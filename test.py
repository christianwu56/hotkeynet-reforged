# from classes import Copy_paste 
# from classes import Parse_commands 
import keyboard
import time

# paste = Copy_paste()

# from Copy_paste Class
def check_command(string):
        if len(string) > 1:
            return True
        return False

# from Copy_paste class
def split_commands(string):
    str_dic = {"strings": [], "space": {"bool": False, "index": None}}
    to_add = ""
    for i in range(len(string)):
        if string[i] == " ":
            str_dic["space"]["bool"] = True
            str_dic["space"]["index"] = i
        if i < len(string) - 1 and (string[i] == string[i + 1] or string.count(string[i]) > 1):
            to_add += string[i]
        else:
            to_add += string[i] + " " + "+" + " "
            str_dic["strings"].append(to_add)
            to_add = ""
        if i == len(string) - 1:
            str_dic["strings"].append(to_add)
    return str_dic

# from Copy_paste class
def paste_text(string):
        # assigns the dictionary return from split_commands to string
        string = split_commands(string)
        # calls check_command and checks if there is more key combo
        # i.e /who can be done without splitting becaues no repeat chars and no spaces
        if not check_command(string["strings"]):
            keyboard.press_and_release(split_commands(string))
            keyboard.press_and_release("enter")
        else:
            # if more than one string in list of strings
            for cmd in string["strings"]:
                # if the first character is not a / then it hits enter to open the text box
                if "/" not in string["strings"][0]:
                    keyboard.press_and_release("enter")
                    print("pressed enter")
                # / opens chat box without pressing enter in most games
                else:
                    # checks if there's a space in the string and if we are in the second to last cmd
                    if string["space"]["bool"] == True and cmd == string["strings"][-2]:
                        # presses spacebar and then the current cmd aka key combo
                        keyboard.press_and_release("spacebar")
                        keyboard.press_and_release(cmd)
                        print(f"pressed {cmd}")
                    else:
                        # if there's no space and not at the 2nd to last index, just press the key combo
                        keyboard.press_and_release(cmd)
                        print(f"pressed {cmd}")
            keyboard.press_and_release("enter")
            print("pressed enter")

time.sleep(5)
print(paste_text("/doability 1"))


