import keyboard
import time
import win32gui


class Copy_paste:
    def __init__(self):
        # delete these once we rework this class's methods to not need them
        s = ""
        s2 = ""
        s3 = " "
        self.s3 = s3
        self.s = s
        self.s2 = s2

    def check_command(self, string):
        for char in string:
            if string.count(char) > 1:
                return True
        return False

    def split_command(self, string):
        string2 = ''
        string3 = ''
        for i in range(len(string) - 1):
            if string[i] == string[i + 1] or string[i] == " " or string.count(string[i]) > 1:
                self.s += string[i]
                string2 = string[i + 1:len(string) + 1]
                break
            if string[i] == string[-1]:
                self.s += string[i]
            else:
                self.s += string[i] + " " + "+" + " "
        if not string2:
            return self.s
        else:
            for i in range(len(string2)):
                if string2[i] == " ":
                    self.s2 += string2[i - 1]
                    string3 = string2[i + 1:len(string2) + 1]
                    break
                elif string2[i + 1] != " ": 
                    self.s2 += string2[i] + " " + "+" + " "
        if not string3:
            return self.s, self.s2
        else:
            for i in range(len(string3)):
                if i == len(string3) - 1:
                    self.s3 += string3[i]
                else:
                    self.s3 += string3[i] + " " + "+" + " "
        
        self.s3 = self.s3.strip()
        return self.s, self.s2, self.s3

    def paste_text(self, string):
        self.split_command(string)
        if not self.check_command(string):
            keyboard.press_and_release(self.s)
            keyboard.press_and_release("enter")
        else:
            if " " in string:
                keyboard.press_and_release(self.s)
                print("pressed: " + self.s)
                keyboard.press_and_release(self.s2)
                print("pressed " + self.s2)
                keyboard.press_and_release("spacebar")
                print("pressed spacebar")
                
                keyboard.press_and_release(self.s3)
                print("pressed " + self.s3)
                keyboard.press_and_release("enter")
            else:
                keyboard.press_and_release(self.s)
                print("pressed: " + self.s)
                keyboard.press_and_release(self.s2)
                print("pressed " + self.s2)
                keyboard.press_and_release("enter")

class Parse_commands(Copy_paste):
    # grabs the current window of from a line in the text file i.e <SendWin EverQuest>
    # sets it as the foreground window when called 
    def set_window(self, title):
        title = ""
        self.title = title
        hwnd = win32gui.FindWindow(None, title)
        try:
            win32gui.SetForegroundWindow(hwnd)
        except not hwnd:
            error = {"message": f"Could not find open window at {title}"}
            return error["message"]

    def execute_commands(self):
        
        def press_and_release(key):
            keyboard.press_and_release(key)
            print(f"Pressed: {key}")

        def wait(duration):
            time.sleep(duration)
            print(f"Waited: {duration}")

        def press(key):
            keyboard.press(key)
            print(f"Pressed: {key}")
        def release(key):
            keyboard.release(key)
            print(f"Released: {key}")

        actions = {
            "<key": press_and_release,
            "<wait": wait,
            "<press": press,
            "<release": release,
            "<SendWin": self.set_window,
            "<text": self.paste_text,
        }
        self.actions = actions
        
        with open("commands.txt", 'r') as f:
            self.commands = f.read().splitlines()
        for command in self.commands:
            print(command)
            if command[0] and command [1] == "/":
                continue
            action = command.split()[0]
            if "<wait" in action:
                    command = float(command.split()[1].strip(">"))
            else:
                command = command.split()[1].strip(">")
            if action in actions:
                func = actions[action]
                func(command)

parse = Parse_commands()
# print(parse.execute_commands())



