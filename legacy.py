# this is a bunch of commented legacy code for fallbacks / reference

# d = {"Tank": ["192.168.1.66", "192.168.1.126"], "Six": "192.168.1.66"}
# for ip in d["Tank"]:

# d = {"Tank": ["192.168.1.66", "192.168.1.126"], "Six": "192.168.1.66"}

# keyboard.add_hotkey('ctrl + [', execute_commands, args =["commands.txt"])
# keyboard.add_hotkey('ctrl + c', stop_listening)


# def execute_commands(file_path):
#     with open(file_path, 'r') as f:
#         commands = f.read().splitlines()
#     for command in commands:
#         # if "<SendWin " in command:
#         #     title = command.split()[1].strip(">")
#         #     activate_window(title)
#         #     time.sleep(0.25)
#         if "<key " in command:
#             key = command.split()[1].strip(">")
#             keyboard.press_and_release(key)
#             confirmation = f'Pressed key: {key}'
#         elif "<wait " in command:
#             wait_time = float(command.split()[1].strip(">"))
#             time.sleep(wait_time)
#             confirmation = f'waited for {wait_time} seconds'
#         elif "<press " in command:
#             key = command.split()[1].strip(">")
#             keyboard.press(key)
#             confirmation = f'Pressed key: {key}'
#         elif "<release " in command:
#             key = command.split()[1].strip(">")
#             keyboard.release(key)
#             confirmation = f'released key: {key}'
#         elif "<text " in command:
#             text = command.split(maxsplit=1)[1].strip(">")
#             time.sleep(0.5)
#             write_text(text)
#             confirmation = f'wrote text: {text}'
#         else:
#             confirmation = f'invalid command: {command}'
#         print(confirmation)

# time.sleep(2)
# activate_window("EverQuest")
# time.sleep(0.5)
# execute_commands("commands.txt")


# tags = {
#             "<key":{
#                 "a": keyboard.press_and_release("a"),
#                 "b": keyboard.press_and_release("b"),
#                 "c": keyboard.press_and_release("c"),
#                 "d": keyboard.press_and_release("d"),
#                 "e": keyboard.press_and_release("e"),
#                 "f": keyboard.press_and_release("f"),
#                 "g": keyboard.press_and_release("g"),
#                 "h": keyboard.press_and_release("h"),
#                 "i": keyboard.press_and_release("i"),
#                 "j": keyboard.press_and_release("j"),
#                 "k": keyboard.press_and_release("k"),
#                 "l": keyboard.press_and_release("l"),
#                 "m": keyboard.press_and_release("m"),
#                 "n": keyboard.press_and_release("n"),
#                 "o": keyboard.press_and_release("o"),
#                 "p": keyboard.press_and_release("p"),
#                 "q": keyboard.press_and_release("q"),
#                 "r": keyboard.press_and_release("r"),
#                 "s": keyboard.press_and_release("s"),
#                 "t": keyboard.press_and_release("t"),
#                 "u": keyboard.press_and_release("u"),
#                 "v": keyboard.press_and_release("v"),
#                 "w": keyboard.press_and_release("w"),
#                 "x": keyboard.press_and_release("x"),
#                 "y": keyboard.press_and_release("y"),
#                 "z": keyboard.press_and_release("z"),
#                 "1": keyboard.press_and_release("1"),
#                 "2": keyboard.press_and_release("2"),
#                 "3": keyboard.press_and_release("3"),
#                 "4": keyboard.press_and_release("4"),
#                 "5": keyboard.press_and_release("5"),
#                 "6": keyboard.press_and_release("6"),
#                 "7": keyboard.press_and_release("7"),
#                 "8": keyboard.press_and_release("8"),
#                 "9": keyboard.press_and_release("9"),
#                 "0": keyboard.press_and_release("0"),
#                 "spacebar": keyboard.press_and_release("spacebar"),
#                 "-": keyboard.press_and_release("-"),
#                 "=": keyboard.press_and_release("="),
#                 "_": keyboard.press_and_release("_"),
#                 "+": keyboard.press_and_release("+"),
#                 "[": keyboard.press_and_release("["),
#                 "{": keyboard.press_and_release("{"),
#                 "}": keyboard.press_and_release("}"),
#                 "]": keyboard.press_and_release("]"),
#                 "\\": keyboard.press_and_release("\\"),
#                 "|": keyboard.press_and_release("|"),
#                 ";": keyboard.press_and_release(";"),
#                 ":": keyboard.press_and_release(":"),
#                 '"': keyboard.press_and_release('"'),
#                 "'": keyboard.press_and_release("'"),
#                 "/": keyboard.press_and_release("/"),
#                 "<": keyboard.press_and_release("<"),
#                 ">": keyboard.press_and_release(">"),
#                 "?": keyboard.press_and_release("?"),
#                 ".": keyboard.press_and_release("."),
#                 ",": keyboard.press_and_release(","),
#                 "alt": keyboard.press_and_release("alt"),
#                 "ctrl": keyboard.press_and_release("ctrl"),
#                 "shift": keyboard.press_and_release("shift"),
#                 "tab": keyboard.press_and_release("tab"),
#                 "enter": keyboard.press_and_release("enter"),
#                 "PgUp": keyboard.press_and_release("PgUp"),
#                 "PgDown": keyboard.press_and_release("PgDown"),
#                 "End": keyboard.press_and_release("End"),
#                 "Del": keyboard.press_and_release("Del"),
#                 "Home": keyboard.press_and_release("Home"),
#                 },
#             "<wait": time.sleep(self.float),
#             "<press": {
#                 "a": keyboard.press("a"),
#                 "b": keyboard.press("b"),
#                 "c": keyboard.press("c"),
#                 "d": keyboard.press("d"),
#                 "e": keyboard.press("e"),
#                 "f": keyboard.press("f"),
#                 "g": keyboard.press("g"),
#                 "h": keyboard.press("h"),
#                 "i": keyboard.press("i"),
#                 "j": keyboard.press("j"),
#                 "k": keyboard.press("k"),
#                 "l": keyboard.press("l"),
#                 "m": keyboard.press("m"),
#                 "n": keyboard.press("n"),
#                 "o": keyboard.press("o"),
#                 "p": keyboard.press("p"),
#                 "q": keyboard.press("q"),
#                 "r": keyboard.press("r"),
#                 "s": keyboard.press("s"),
#                 "t": keyboard.press("t"),
#                 "u": keyboard.press("u"),
#                 "v": keyboard.press("v"),
#                 "w": keyboard.press("w"),
#                 "x": keyboard.press("x"),
#                 "y": keyboard.press("y"),
#                 "z": keyboard.press("z"),
#                 "1": keyboard.press("1"),
#                 "2": keyboard.press("2"),
#                 "3": keyboard.press("3"),
#                 "4": keyboard.press("4"),
#                 "5": keyboard.press("5"),
#                 "6": keyboard.press("6"),
#                 "7": keyboard.press("7"),
#                 "8": keyboard.press("8"),
#                 "9": keyboard.press("9"),
#                 "0": keyboard.press("0"),
#                 "spacebar": keyboard.press("spacebar"),
#                 "-": keyboard.press("-"),
#                 "=": keyboard.press("="),
#                 "_": keyboard.press("_"),
#                 "+": keyboard.press("+"),
#                 "[": keyboard.press("["),
#                 "{": keyboard.press("{"),
#                 "}": keyboard.press("}"),
#                 "]": keyboard.press("]"),
#                 "\\": keyboard.press("\\"),
#                 "|": keyboard.press("|"),
#                 ";": keyboard.press(";"),
#                 ":": keyboard.press(":"),
#                 '"': keyboard.press('"'),
#                 "'": keyboard.press("'"),
#                 "/": keyboard.press("/"),
#                 "<": keyboard.press("<"),
#                 ">": keyboard.press(">"),
#                 "?": keyboard.press("?"),
#                 ".": keyboard.press("."),
#                 ",": keyboard.press(","),
#                 "alt": keyboard.press("alt"),
#                 "ctrl": keyboard.press("ctrl"),
#                 "shift": keyboard.press("shift"),
#                 "tab": keyboard.press("tab"),
#                 "enter": keyboard.press("enter"),
#                 "PgUp": keyboard.press("PgUp"),
#                 "PgDown": keyboard.press("PgDown"),
#                 "End": keyboard.press("End"),
#                 "Del": keyboard.press("Del"),
#                 "Home": keyboard.press("Home"),
#                 },
#             "<release": {
#                 "a": keyboard.release("a"),
#                 "b": keyboard.release("b"),
#                 "c": keyboard.release("c"),
#                 "d": keyboard.release("d"),
#                 "e": keyboard.release("e"),
#                 "f": keyboard.release("f"),
#                 "g": keyboard.release("g"),
#                 "h": keyboard.release("h"),
#                 "i": keyboard.release("i"),
#                 "j": keyboard.release("j"),
#                 "k": keyboard.release("k"),
#                 "l": keyboard.release("l"),
#                 "m": keyboard.release("m"),
#                 "n": keyboard.release("n"),
#                 "o": keyboard.release("o"),
#                 "p": keyboard.release("p"),
#                 "q": keyboard.release("q"),
#                 "r": keyboard.release("r"),
#                 "s": keyboard.release("s"),
#                 "t": keyboard.release("t"),
#                 "u": keyboard.release("u"),
#                 "v": keyboard.release("v"),
#                 "w": keyboard.release("w"),
#                 "x": keyboard.release("x"),
#                 "y": keyboard.release("y"),
#                 "z": keyboard.release("z"),
#                 "1": keyboard.release("1"),
#                 "2": keyboard.release("2"),
#                 "3": keyboard.release("3"),
#                 "4": keyboard.release("4"),
#                 "5": keyboard.release("5"),
#                 "6": keyboard.release("6"),
#                 "7": keyboard.release("7"),
#                 "8": keyboard.release("8"),
#                 "9": keyboard.release("9"),
#                 "0": keyboard.release("0"),
#                 "spacebar": keyboard.release("spacebar"),
#                 "-": keyboard.release("-"),
#                 "=": keyboard.release("="),
#                 "_": keyboard.release("_"),
#                 "+": keyboard.release("+"),
#                 "[": keyboard.release("["),
#                 "{": keyboard.release("{"),
#                 "}": keyboard.release("}"),
#                 "]": keyboard.release("]"),
#                 "\\": keyboard.release("\\"),
#                 "|": keyboard.release("|"),
#                 ";": keyboard.release(";"),
#                 ":": keyboard.release(":"),
#                 '"': keyboard.release('"'),
#                 "'": keyboard.release("'"),
#                 "/": keyboard.release("/"),
#                 "<": keyboard.release("<"),
#                 ">": keyboard.release(">"),
#                 "?": keyboard.release("?"),
#                 ".": keyboard.release("."),
#                 ",": keyboard.release(","),
#                 "alt": keyboard.release("alt"),
#                 "ctrl": keyboard.release("ctrl"),
#                 "shift": keyboard.release("shift"),
#                 "tab": keyboard.release("tab"),
#                 "enter": keyboard.release("enter"),
#                 "PgUp": keyboard.release("PgUp"),
#                 "PgDown": keyboard.release("PgDown"),
#                 "End": keyboard.release("End"),
#                 "Del": keyboard.release("Del"),
#                 "Home": keyboard.release("Home"),
#                 },
#             # dont know if these two entries works like i want
#             "<text": self.paste_text(self.string),
#             "<SendWin": self.set_window(self.title)
#         }

# Legacy execute_commands
    # def execute_commands(self):
    #     # grabs the commands
    #     self.commands = self.grab_commands("commands.txt")
    #     # dictionary of all the key inputs (for now) 
    #         # dont know if these two entries works like i want
    #     self.tags = tags
    #     for command in self.commands:
    #         # checks if the keyword is in command 
    #         if "<wait" in command:
    #             # splits it into a list of two items
    #             # example - ["<wait", "0.5>"]
    #             self.float = float(command.split()[1].strip(">"))
    #             self.tags[command.split()](self.float)
    #         elif "<SendWin" in command:
    #             # example - ["<SendWin", "EverQuest>"]
    #             self.title == command.split()[1].strip(">")
    #             self.set_window(self.title)
    #         elif "<text" in command:
    #             # example - ["<Text", "/attack>"]
    #             self.string = command.split()[1].strip(">")
    #             self.tags[command](self.string)
    #         elif "<wait" not in command or "<SendWin" not in command or "<Text" not in command:
    #             # example - ["<key", "a>"]
    #             # example - ["<press", "a>"]
    #             # example - ["<release", "a>"]
    #             # tags["<key"]["a"]  --> keyboard.press_and_release("a")
    #             tags[command.split()[0]][command.split()[1].strip(">")]
    
    

# grabs the commands
        # self.commands = self.grab_commands("commands.txt")

# grabs the command from the text file at file_path
    # and returns it with each line in the file split
    # def grab_commands(self, file_path):
    #     self.file_path = file_path
    #     # opens the file as read
    #     with open(file_path, 'r') as file:
    #         commands = file.read().splitlines()
    #         self.commands = commands
    #     print(self.commands)
    #     return self.commands, file_path

# keyboard.add_hotkey('ctrl + [', execute_commands, args =["commands.txt"])
# keyboard.add_hotkey('ctrl + c', stop_listening)